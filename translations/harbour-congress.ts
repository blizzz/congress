<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <source>../data/about.txt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>days until #37c3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DayList</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DayPage</name>
    <message>
        <source>Day </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventListItem</name>
    <message>
        <source>Duration: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventPage</name>
    <message>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stream video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <source>Set video path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please note that a video may take GBs of space! Please prefer sdcard path.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpeakerPage</name>
    <message>
        <source>Events:
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpeakersPage</name>
    <message>
        <source>Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VidPathDialog</name>
    <message>
        <source>Set Videopath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set video path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please note that a video may take GBs of space! Please prefer sdcard path.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoListItem</name>
    <message>
        <source>Duration: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Downloaded Videos</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
